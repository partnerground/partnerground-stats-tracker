const call = require('../helper/call');

module.exports = ({
                    linkVisitLogSessionId,
                  }) => {
  const payload = {
    linkVisitLogSessionId,
  };

  // Promise
  return call({
    endpoint: 'track/pageview',
    payload,
  });
};

