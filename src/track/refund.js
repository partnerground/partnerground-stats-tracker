const call = require('../helper/call');

module.exports = ({
                    linkVisitLogSessionId,
                    amountCents,
                    itemCustomGroup,
                    itemQuantity,
                  }) => {
  const payload = {
    linkVisitLogSessionId,
    amountCents,
    itemCustomGroup,
    itemQuantity,
  };

  // Promise
  return call({
    endpoint: 'track/refund',
    payload,
  });
};

