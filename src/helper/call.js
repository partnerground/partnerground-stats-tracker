const fetch = require('isomorphic-unfetch');

function call({endpoint, payload, headers, httpMethod}) {
  return new Promise(async (resolve) => {
    httpMethod = httpMethod || 'POST';

    const url = `https://partnerground.com/api/v3/${endpoint}`;
    payload = payload || {};

    const headersForFetch = {};

    // set headers
    if (headers && headers.length > 0) {
      headers.forEach((header) => {
        if (header.name && header.value) {
          headersForFetch[header.name] = header.value;
        }
      });
    }

    let rawResult = null;
    let result = null;

    try {
      headersForFetch['Accept'] = 'application/json';
      headersForFetch['Content-Type'] = 'application/json';

      rawResult = await fetch(url, {
        method: httpMethod,
        headers: headersForFetch,
        body: JSON.stringify(payload),
      });
    } catch (e) {
      console.error(`#354TRFDF API request failed:  `, e);
      result = {error: {code: '#3REFDH353F', message: 'CHECK_YOUR_CONNECTION'}, data: null};
    }

    let parsedResult = {};
    if (rawResult) {
      const _rawResultText = await rawResult.text();
      try {
        parsedResult = JSON.parse(_rawResultText);
      } catch (e) {
        console.error(`#54WRFDTRJ API_CALL_FAILED: `, e, _rawResultText);
        result = {error: {code: '#REFDFHTRDF', message: 'API_CALL_FAILED'}, data: null};
      }
    }

    if (rawResult && rawResult.ok) {
      try {
        result = parsedResult;
      } catch (e) {
        // it was not a JSON
        const detectHtml = result ? result : '';
        if (
          detectHtml
            .toString()
            .trim()
            .indexOf('<') === 0
        ) {
          result = {error: {code: '#HJY756RTD', message: 'INTERNAL_ERROR'}, data: null};
        }
      }

      resolve(result);
    } else {
      // status code not 2xx
      resolve(result || {error: {code: '#0VHC9KOJ8', message: parsedResult}, data: null});
    }
  });
}

module.exports = call;