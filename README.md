This is NPM module which exposes methods for easier integration with PartnerGround.com.
It allows to track statistical information for affiliate links.

https://partnerground.com/

## Usage overview

When PartnerGround redirects user to your site URL will have added query parameter `_partnerground_vls`

Application must save this `_partnerground_vls` and pass it in to track methods on client or server side as `linkVisitLogSessionId`.

>Pageview method must always be used on client side.

## Methods to track stats

```javascript
const tracker = require('@partnerground/stats-tracker');

// Client side only
tracker.pageview({
    linkVisitLogSessionId, // Required. Identifies the visit session. 
});

// Client or server side
tracker.addToCart({
    linkVisitLogSessionId, // Required. Identifies the visit session. 
    amountCents,    // Required. Value is in cents. $1 = 100. Used for stats to count purchase amounts.
    itemCustomGroup,// Required. Item group, same as you send to create the reward. Used for stats.
    itemQuantity,   // Required. Used for stats to count sales count.
});

// Client or server side
tracker.removeFromCart({
    linkVisitLogSessionId, // Required. Identifies the visit session. 
    amountCents,    // Required. Value is in cents. $1 = 100. Used for stats to count purchase amounts.
    itemCustomGroup,// Required. Item group, same as you send to create the reward. Used for stats.
    itemQuantity,   // Required. Used for stats to count sales count.
});

// Client or server side
tracker.purchase({
    linkVisitLogSessionId, // Required. Identifies the visit session. 
    amountCents,    // Required. Value is in cents. $1 = 100. Used for stats to count purchase amounts.
    itemCustomGroup,// Required. Item group, same as you send to create the reward. Used for stats.
    itemQuantity,   // Required. Used for stats to count sales count.
});

// Client or server side
tracker.refund({
    linkVisitLogSessionId, // Required. Identifies the visit session. 
    amountCents,    // Required. Value is in cents. $1 = 100. Used for stats to count purchase amounts.
    itemCustomGroup,// Required. Item group, same as you send to create the reward. Used for stats.
    itemQuantity,   // Required. Used for stats to count sales count.
});
```
