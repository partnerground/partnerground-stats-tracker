module.exports = {
  pageview: require('./src/track/pageview'),
  addToCart: require('./src/track/addToCart'),
  removeFromCart: require('./src/track/removeFromCart'),
  purchase: require('./src/track/purchase'),
  refund: require('./src/track/refund'),
};
